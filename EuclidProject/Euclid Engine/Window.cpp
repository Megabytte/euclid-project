#include "Window.h"

namespace me
{
	Window::Window()
	{
		Window(100, 100, 100, 100, "A Window");
	}

	Window::Window(float posX, float posY, float sizeX, float sizeY, std::string windowName)
	{
		window = sfg::Window::Create();
		//window->SetPosition(sf::Vector2f(posX, posY));
		//window->SetRequisition(sf::Vector2f(sizeX, sizeY));
		window->SetTitle(windowName);
	}


	Window::~Window()
	{
	}
}
