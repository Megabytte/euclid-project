#include "ResourceManager.h"
#include "ResourcePath.hpp"

ResourceManager* ResourceManager::s_pInstance = 0;

ResourceManager::ResourceManager()
{
	tempT.loadFromFile(resourcePath() + "MissTex.png");
	textures.insert(std::make_pair("MissTex", tempT));

	mArray[0].openFromFile(resourcePath() + "MissSound.ogg");
	mArray[0].setLoop(true);
	music.insert(std::make_pair("MissingSound", &mArray[0]));
	tempA.loadFromFile(resourcePath() + "MissSound.ogg");
	soundB.insert(std::make_pair("MissingSound", tempA));
	sf::Sound t;
	t.setBuffer(soundB.find("MissingSound")->second);
	sounds.insert(std::make_pair("MissingSound", t));
}

ResourceManager::~ResourceManager()
{

}

sf::Texture& ResourceManager::getTexture(std::string id)
{
	if (textures.find(id) == textures.end())
	{
		std::cout << "\nGetting Texture With ID: " << id << " Failed!\n";
		return textures.find("MissTex")->second;
	}
	else
		return textures.find(id)->second;
}

bool ResourceManager::loadTexture(std::string id, std::string path)
{
	if (tempT.loadFromFile(resourcePath() + path))
	{
		textures.insert(make_pair(id, tempT));
	}
	else
	{
		std::cout << "\nLoading File: " << path << " With ID: " << id << " Failed!\n";
		return false;
	}

	return true;
}

bool ResourceManager::unloadTexture(std::string id)
{
	if (textures.find(id) == textures.end())
	{
		std::cout << "Unloading Texture With ID: " << id << " Failed!\n";
		return false;
	}
	else
	{
		textures.erase(id);
		return true;
	}
}

void ResourceManager::freeAllTextures()
{
    textures.clear();
}

void ResourceManager::freeAllSounds()
{
    soundB.clear();
    sounds.clear();
}

bool ResourceManager::loadMusic(std::string id, std::string path)
{
	if (numOfMusic != 19)
	{
		if (mArray[numOfMusic].openFromFile(resourcePath() + path))
		{
			music.insert(std::make_pair(id, &mArray[numOfMusic]));
			numOfMusic++;
			return true;
		}
		else
		{
			std::cout << "\nLoading Music with ID: " << id << " and Path: " << path << " failed!\n";
			return false;
		}
	}
	else
	{
		return false;
	}
}

bool ResourceManager::unloadMusic(std::string id)
{
	//TODO: Make Method
    if (music.find(id) == music.end())
    {
        std::cout << "\nUnloading Music with ID: " << id << " failed!\n";
    }
    else
    {
        delete music.find(id)->second;
    }
    
	return false;
}

void ResourceManager::playMusic(std::string id)
{
	if (music.find(id) == music.end())
	{
		std::cout << "\nPlaying Music with ID: " << id << " failed!\n";
	}
	else
	{
		music.find(id)->second->play();
	}
}

void ResourceManager::pauseMusic(std::string id)
{
	if (music.find(id) == music.end())
	{
		std::cout << "\nPausing Music with ID: " << id << " failed!\n";
	}
	else
	{
		music.find(id)->second->pause();
	}
}

sf::Music& ResourceManager::getMusic(std::string id)
{
	if (music.find(id) == music.end())
	{
		std::cout << "\nGetting Music with ID: " << id << " failed!\n";
		return *music.find("MissingSound")->second;
	}
	else
	{
		return *music.find(id)->second;
	}
}

bool ResourceManager::loadSound(std::string id, std::string path)
{
	sf::Sound s;
	if (tempA.loadFromFile(resourcePath() + path))
	{
		soundB.insert(std::make_pair(id, tempA));
		s.setBuffer(soundB.find(id)->second);
		sounds.insert(std::make_pair(id, s));
	}
	else
	{
		std::cout << "\nLoading Sound: " << path << " with ID: " << id << " failed!\n";
		return false;
	}

	return true;
}

bool ResourceManager::unloadSound(std::string id)
{
	if (sounds.find(id) == sounds.end() && soundB.find(id) == soundB.end())
	{
		std::cout << "\nUnloading Sound With ID: " << id << " Failed!\n";
		return false;
	}
	else if (sounds.find(id) != sounds.end() && soundB.find(id) != soundB.end())
	{
		sounds.erase(id);
		soundB.erase(id);
		return true;
	}
	else
		return false;
}

void ResourceManager::playSound(std::string id)
{
	if (sounds.find(id) == sounds.end())
	{
		std::cout << "\nPlaying Sound with ID: " << id << " failed!\n";
	}
	else
	{
		sounds.find(id)->second.play();
	}
}

void ResourceManager::pauseSound(std::string id)
{
	if (sounds.find(id) == sounds.end())
	{
		std::cout << "\nPausing Sound with ID: " << id << " failed!\n";
	}
	else
	{
		sounds.find(id)->second.pause();
	}
}

sf::Sound& ResourceManager::getSound(std::string id)
{
	if (sounds.find(id) == sounds.end())
	{
		std::cout << "\nGetting Sound with ID: " << id << " failed!\n";
		return sounds.find("MissingSound")->second;
	}
	else
	{
		return sounds.find(id)->second;
	}
}
