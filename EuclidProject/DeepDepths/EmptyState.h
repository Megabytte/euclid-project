//
//  EmptyState.h
//  EuclidProject
//
//  Created by Keith Webb on 11/4/14.
//  Copyright (c) 2014 Keith Webb. All rights reserved.
//

#pragma once

#include "GameState.h"

class EmptyState : public GameState
{
public:
    EmptyState(Game* game) { this->game = game; }
    EmptyState() { game = nullptr; }
    
    virtual void loadAssets();
    virtual void draw(const float dt);
    virtual void update(const float dt);
    virtual void handleInput();
};

