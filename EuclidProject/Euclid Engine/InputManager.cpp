#include "InputManager.h"

InputManager* InputManager::s_pInstance = 0;

InputManager::InputManager()
{

}

InputManager::~InputManager()
{

}

bool InputManager::EventTriggered(sf::Event event, sf::Event::EventType type)
{
	if (event.type == type)
	{
		return true;
	}
	else
		return false;
}

bool InputManager::isKeyDown(sf::Keyboard::Key key)
{
    if(sf::Keyboard::isKeyPressed(key))
        return true;
    else
        return false;
}

bool InputManager::isKeyDownEvent(sf::Event event, sf::Keyboard::Key key)
{
    if(event.key.code == key)
        return true;
    else
        return false;
}
