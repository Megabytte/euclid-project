#pragma once

#include <SFGUI/SFGUI.hpp>

namespace me
{
	class Window
	{
	public:
		Window();
		Window(float posX, float posY, float sizeX, float sizeY, std::string windowName);
		~Window();

		sfg::Window::Ptr getWindow() const { return window; }

	private:
		sfg::Window::Ptr window;
	};
}
