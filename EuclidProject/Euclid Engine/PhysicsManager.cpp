#include "PhysicsManager.h"

PhysicsManager* PhysicsManager::s_pInstance = 0;

PhysicsManager::PhysicsManager()
{

}

PhysicsManager::~PhysicsManager()
{
    //delete world;
}

void PhysicsManager::setUpPhysics(float timeStep, int velocityIterations, int positionIterations, b2Vec2 gravity)
{
	this->timeStep = timeStep;
	this->velocityIterations = velocityIterations;
	this->positionIterations = positionIterations;
    
    Ptr q(new b2World(gravity));
    this->world = std::move(q);
    
	this->setup = true;
}

void PhysicsManager::update()
{
	if (setup)
		world->Step(timeStep, velocityIterations, positionIterations);
}
