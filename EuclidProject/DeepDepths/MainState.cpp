#include "MainState.h"
#include "EmptyState.h"

#include "RNG.h"

MainState::MainState(Game* game)
{
	this->game = game;
    //std::cout << "\nI've been created!";
}

MainState::~MainState()
{
    //std::cout << "\nI've been deleted!";
    TRM::Get()->freeAllTextures();
    TRM::Get()->freeAllSounds();
    this->game = nullptr;
}

void MainState::draw(const float dt)
{
	this->game->window.clear(sf::Color::Black);
	TPM::Get()->getWorld()->DrawDebugData();
	
	caveman.draw(this->game->window);

	return;
}

void MainState::update(const float dt)
{
	//TODO: Update Here
	TPM::Get()->update();
    
	caveman.update(this->game->elapsed);

	return;
}

void MainState::handleInput()
{
	sf::Event event;
    
	while (this->game->window.pollEvent(event))
	{
		//TODO: Check For Input Here
		this->game->desktop.HandleEvent(event);

		if (TIM::Get()->EventTriggered(event, sf::Event::Closed))
		{
			game->window.close();
		}
        
        if (TIM::Get()->isKeyDown(sf::Keyboard::O))
        {
            caveman.getSprite().play();
        }
        if (TIM::Get()->isKeyDown(sf::Keyboard::P))
        {
            caveman.getSprite().pause();
        }
        
        if (TIM::Get()->isKeyDown(sf::Keyboard::Space))
        {
            this->game->desktop.RemoveAll();
            game->changeState(new EmptyState(game));
            //std::cout << "\nChanged!";
            break;
        }
        
#ifdef SFML_SYSTEM_MACOS
        if (TIM::Get()->isKeyDown(sf::Keyboard::LSystem))
        {
            if(TIM::Get()->isKeyDownEvent(event, sf::Keyboard::Q))
            {
                game->window.close();
            }
        }
#endif
	}
    
	return;
}

void MainState::loadAssets()
{
	//TODO: Load Assets Here
	TPM::Get()->setUpPhysics(1.0f / 60.0f, 6, 2, b2Vec2(0, 10));
	TPM::Get()->getWorld()->SetAllowSleeping(false);

	this->spinner = sfg::SpinButton::Create(-10.0f, 10.0f, 1.0f);
	spinner->GetSignal(sfg::SpinButton::OnValueChanged).Connect(
		std::bind(&MainState::OnSpinBClick, this)
	);
	spinner->SetDigits(0);
	spinner->SetRequisition(sf::Vector2f(50.0f, 20.0f));

	this->button = sfg::Button::Create("A Button");
	button->GetSignal(sfg::Button::OnLeftClick).Connect(
		std::bind(&MainState::OnButtonClick, this)
	);

	table = sfg::Table::Create();

	window = me::Window(0, 0, 200, 200, "Yolo");
	window.getWindow()->Add(table);
	table->Attach(spinner, sf::Rect<sf::Uint32>(0, 0, 1, 2));
	table->Attach(button, sf::Rect<sf::Uint32>(1, 0, 2, 2));
	this->game->desktop.Add(window.getWindow());

	TRM::Get()->loadSound("power", "powerUp.wav");

	floor = PhysicsObject::createAsBox(TPM::Get()->getWorld(), b2_staticBody, b2Vec2(0, 400.0f), b2Vec2(800.0f, 10.0f), 0.0f, 0.0f, 0.3f);
	cube = PhysicsObject::createAsBox(TPM::Get()->getWorld(), b2_dynamicBody, b2Vec2(400.0f, 10), b2Vec2(10.0f, 10.0f), 0.0f, 0.3f, 0.3f);
	sphere = PhysicsObject::createAsSphere(TPM::Get()->getWorld(), b2_dynamicBody, b2Vec2(410.0f, -10), 5.0f, 0.4f, 0.3f, 0.3f);
    
    std::unique_ptr<DebugDraw> temp(new DebugDraw(this->game->window));
    debugD = std::move(temp);
    TPM::Get()->getWorld()->SetDebugDraw(debugD.get());
    debugD->SetFlags(b2Draw::e_shapeBit);
    temp.release();

	TRM::Get()->loadTexture("Anim", "Test.png");
	TRM::Get()->loadTexture("Cave", "running_e0000_3x3.png");

	caveman.createNewAnimation("runEast", "Cave", 96, 96, 2, 2, 8, GameObject::LEFT_TO_RIGHT_TOP_TO_BOTTOM);
	caveman.getSprite().play(caveman.getAnimation("runEast"));
	caveman.getSprite().setFrameTime(sf::milliseconds(100));
	caveman.getSprite().setPosition(400, 100);
	caveman.getSprite().setScale(2.0f, 2.0f);

	this->setLoaded(true);
	return;
}

void MainState::OnSpinBClick()
{
	TPM::Get()->getWorld()->SetGravity(b2Vec2(0, (float32)this->spinner->GetValue()));
}

void MainState::OnButtonClick()
{
	//sphere.getBody()->ApplyLinearImpulse(b2Vec2(0.1f, 0.1f), b2Vec2(2.0f, 1.0f), true);
	sphere.getBody()->ApplyForce(b2Vec2(0.4f, 0.0f), sphere.getBody()->GetWorldPoint(b2Vec2(0.5f, 0.5f)), true);
}
