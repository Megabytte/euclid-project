#pragma once

#include "Game.h"

class GameState
{
public:
	Game* game;
	//sf::View view;

	bool getLoaded() const { return loaded; }
	void setLoaded(bool val) { loaded = val; }

	virtual void loadAssets() = 0;
	virtual void update(const float dt) = 0;
	virtual void handleInput() = 0;
	virtual void draw(const float dt) = 0;
    
    virtual ~GameState() { game = nullptr; }

private:
	bool loaded = false;
};

