#define SCREEN_WIDTH 800
#define SCREEN_HEIGHT 600
#define APPLICATION_NAME "B Game"
#define ICON_PATH "icon.png"

#include "Game.h"
#include "MainState.h"

int main()
{
	Game game(SCREEN_WIDTH, SCREEN_HEIGHT, APPLICATION_NAME, ICON_PATH);
	game.pushState(new MainState(&game));
	game.gameLoop();

	return 0;
}