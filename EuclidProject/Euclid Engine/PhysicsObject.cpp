#include "PhysicsObject.h"

PhysicsObject::PhysicsObject(b2World* world, b2BodyType type, b2Vec2 position, b2Vec2 boxSize, float density, float restitution, float friction)
{
	//Box Creation
	b2BodyDef bodyDef;
	b2PolygonShape polygonShape;
	b2FixtureDef fixtureDef;

	bodyDef.type = type;
	bodyDef.position.Set(position.x / 30.0f, position.y / 30.0f);
    //std::unique_ptr<b2Body> temp(world->CreateBody(&bodyDef));
    this->body = world->CreateBody(&bodyDef);
	polygonShape.SetAsBox(boxSize.x / 30.0f, boxSize.y / 30.0f);
	fixtureDef.shape = &polygonShape;
	fixtureDef.density = density;
	fixtureDef.friction = friction;
	fixtureDef.restitution = restitution;
	this->body->CreateFixture(&fixtureDef);
}

PhysicsObject::PhysicsObject(b2World* world, b2BodyType type, b2Vec2 position, float radius, float density, float restitution, float friction)
{
	//Sphere Creation
	b2BodyDef bodyDef;
	b2CircleShape circleShape;
	b2FixtureDef fixtureDef;

	bodyDef.type = type;
	bodyDef.position.Set(position.x / 30.0f, position.y / 30.0f);
    //std::unique_ptr<b2Body> temp(world->CreateBody(&bodyDef));
    this->body = world->CreateBody(&bodyDef);
	circleShape.m_radius = radius / 30.0f;
	fixtureDef.shape = &circleShape;
	fixtureDef.density = density;
	fixtureDef.friction = friction;
	fixtureDef.restitution = restitution;
	this->body->CreateFixture(&fixtureDef);
}

PhysicsObject::~PhysicsObject()
{
    body = nullptr;
}
