//
//  SceneNode.h
//  EuclidProject
//
//  Created by Keith Webb on 11/5/14.
//  Copyright (c) 2014 Keith Webb. All rights reserved.
//

#pragma once

#include <memory>
#include <vector>

#include <SFML/Graphics.hpp>

class SceneNode : public sf::Transformable, public sf::Drawable, private sf::NonCopyable
{
public:
    typedef std::unique_ptr<SceneNode> Ptr;
    
public:
    SceneNode() {}
    ~SceneNode() { mParent = nullptr; }
    
    void attachChild(Ptr child);
    Ptr detachChild(const SceneNode& node);
    
private:
    virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
    virtual void drawCurrent(sf::RenderTarget& target, sf::RenderStates states) const;
    
private:
    std::vector<Ptr> mChildren;
    SceneNode* mParent;
};
