#include "Game.h"
#include "GameState.h"
#include "ResourcePath.hpp"

#include <cstdlib>
#include <ctime>

bool Game::stateChange = false;

Game::Game(int window_w, int window_h, std::string name, std::string iconPath)
{
	this->window.create(sf::VideoMode(window_w, window_h), name);
	this->window.setFramerateLimit(60);
    
    sf::Image icon;
    if (!icon.loadFromFile(resourcePath() + iconPath)) {
        return;
    }
    else
        window.setIcon(icon.getSize().x, icon.getSize().y, icon.getPixelsPtr());
    
    srand (static_cast <unsigned> (time(0)));
}

Game::~Game()
{
	while (!this->states.empty())
		popState();
}

void Game::pushState(GameState* state)
{
	this->states.push(state);
	return;
}

void Game::popState()
{
	delete this->states.top();
	this->states.pop();
	return;
}

void Game::changeState(GameState* state)
{
	if (!this->states.empty())
		popState();
	pushState(state);
    stateChange = true;
	return;
}

GameState* Game::peekState()
{
	if (this->states.empty())
        return nullptr;
	return this->states.top();
}

void Game::gameLoop()
{
	sf::Clock clock;

	while (this->window.isOpen())
	{
		elapsed = clock.restart();
		float dt = elapsed.asSeconds();

		if (peekState() == nullptr)
			continue;

		this->window.clear(sf::Color::Black);

		if (peekState()->getLoaded() == false)
		{
			peekState()->loadAssets();
		}
		else
		{
			peekState()->handleInput();
            if(!stateChange) {
                desktop.Update(desktopClock.restart().asSeconds());
                peekState()->update(dt);
                peekState()->draw(dt);
                sfgui.Display(window);
            }
            
            if(stateChange)
                stateChange = false;
		}

		window.display();
	}
}