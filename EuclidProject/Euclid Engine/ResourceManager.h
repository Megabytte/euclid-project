#pragma once

#include <map>
#include <string>
#include <iostream>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>

class ResourceManager
{
public:
	static ResourceManager* Get()
	{
		if (s_pInstance == 0)
		{
			s_pInstance = new ResourceManager();
			return s_pInstance;
		}
		return s_pInstance;
	}

	sf::Texture& getTexture(std::string id);
	bool loadTexture(std::string id, std::string path);
	bool unloadTexture(std::string id);

	bool loadMusic(std::string id, std::string path);
	bool unloadMusic(std::string id);
	void playMusic(std::string id);
	void pauseMusic(std::string id);
	sf::Music& getMusic(std::string id);

	bool loadSound(std::string id, std::string path);
	bool unloadSound(std::string id);
	void playSound(std::string id);
	void pauseSound(std::string id);
	sf::Sound& getSound(std::string id);
    
    void freeAllTextures();
    void freeAllSounds();
	
private:
	ResourceManager();
	~ResourceManager();

	sf::Texture tempT;
	std::map<std::string, sf::Texture> textures;

	sf::SoundBuffer tempA;
	std::map<std::string, sf::SoundBuffer> soundB;
	std::map<std::string, sf::Sound> sounds;

	std::map<std::string, sf::Music*> music;
	int numOfMusic = 1;
	sf::Music mArray[20];

	static ResourceManager* s_pInstance;
};
typedef ResourceManager TRM;
