//
//  RNG.h
//  EuclidProject
//
//  Created by Keith Webb on 11/5/14.
//  Copyright (c) 2014 Keith Webb. All rights reserved.
//

#pragma once

#include <random>

class RNG
{
public:
    static int genIntBetweenAB(int a, int b)
    {
        if(a-b < 0)
            return (rand() % ((a - b) - 1) + a);
        else
            return 1;
    }
    static float genFloatBetweenAB(float a, float b)
    {
        return a + static_cast <float> (rand()) / ( static_cast <float> (RAND_MAX/(b-a)));
    }
};
