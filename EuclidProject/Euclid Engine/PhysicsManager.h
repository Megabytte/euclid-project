#pragma once

#include "Box2D/Box2D.h"

class PhysicsManager
{
public:
	static PhysicsManager* Get()
	{
		if (s_pInstance == 0)
		{
			s_pInstance = new PhysicsManager();
			return s_pInstance;
		}
		return s_pInstance;
	}

	float getTimeStep() const { return timeStep; }
	void setTimeStep(float val) { timeStep = val; }
	int getVelocityIterations() const { return velocityIterations; }
	void setVelocityIterations(int val) { velocityIterations = val; }
	int getPositionIterations() const { return positionIterations; }
	void setPositionIterations(int val) { positionIterations = val; }

	b2World* getWorld() { return world.get(); }

	void setUpPhysics(float timeStep = 1.0f / 60.0f, int velocityIterations = 6, int positionIterations = 2, b2Vec2 gravity = b2Vec2(0.0f, 10.0f));
	void update();

private:
    typedef std::unique_ptr<b2World> Ptr;
    
	PhysicsManager();
	~PhysicsManager();
	bool setup = false;
	
	float timeStep;
	int velocityIterations;
	int positionIterations;

//	b2World world;
    Ptr world;

	static PhysicsManager* s_pInstance;
};
typedef PhysicsManager TPM;


