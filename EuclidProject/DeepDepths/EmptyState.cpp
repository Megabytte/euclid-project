//
//  EmptyState.cpp
//  EuclidProject
//
//  Created by Keith Webb on 11/4/14.
//  Copyright (c) 2014 Keith Webb. All rights reserved.
//

#include "EmptyState.h"
#include "InputManager.h"
#include "MainState.h"

void EmptyState::loadAssets()
{
    this->setLoaded(true);
    return;
}

void EmptyState::draw(const float dt)
{
    this->game->window.clear(sf::Color::Black);
}

void EmptyState::update(const float dt)
{
    
}

void EmptyState::handleInput()
{
    sf::Event event;
    
    while (this->game->window.pollEvent(event))
    {
        if (TIM::Get()->EventTriggered(event, sf::Event::Closed))
        {
            game->window.close();
        }
        
        if (TIM::Get()->isKeyDown(sf::Keyboard::Return))
        {
            game->changeState(new MainState(game));
            break;
        }
    }
}
