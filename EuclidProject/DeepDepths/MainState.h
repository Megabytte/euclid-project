#pragma once

#include "ResourceManager.h"
#include "InputManager.h"
#include "PhysicsManager.h"
#include "PhysicsObject.h"
#include "GameState.h"
#include <SFGUI/SFGUI.hpp>
#include <memory>
#include "DebugRenderer.h"
#include "Window.h"
#include "GameObject.h"
#include "Animation.h"
#include "AnimatedSprite.h"

class MainState : public GameState
{
public:
    
    MainState(Game* game);
    ~MainState();

	virtual void loadAssets();
	virtual void draw(const float dt);
	virtual void update(const float dt);
	virtual void handleInput();

	void OnSpinBClick();
	void OnButtonClick();

	PhysicsObject floor;
	PhysicsObject cube;
	PhysicsObject sphere;

	GameObject caveman;
	
    std::unique_ptr<DebugDraw> debugD;
    
	me::Window window;

	sfg::Table::Ptr table;
	sfg::Button::Ptr button;
	sfg::SpinButton::Ptr spinner;
};