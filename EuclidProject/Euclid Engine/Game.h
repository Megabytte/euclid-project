#pragma once

#include <stack>
#include <map>
#include <string>
#include <SFML/Graphics.hpp>
#include <SFGUI/SFGUI.hpp>

class GameState;

class Game
{
public:
	std::stack<GameState*> states;

	sf::RenderWindow window;
	sfg::SFGUI sfgui;

	sf::Clock desktopClock;
	sfg::Desktop desktop;
	sf::Time elapsed;

	void pushState(GameState* state);
	void popState();
	void changeState(GameState* state);
	GameState* peekState();

	void gameLoop();
    
    static bool stateChange;
    
    Game(int window_w, int window_h, std::string name, std::string iconPath);
	~Game();
};

