#pragma once

#include "Box2D/Box2D.h"

class PhysicsObject
{
public:
	static PhysicsObject createAsBox(b2World* world, b2BodyType type = b2_staticBody, b2Vec2 position = b2Vec2(0, 0), b2Vec2 boxSize = b2Vec2(1, 1), float density = 0, float restitution = 0, float friction = 0.3f)
	{
		return PhysicsObject(world, type, position, boxSize, density, restitution, friction);
	}
	static PhysicsObject createAsSphere(b2World* world, b2BodyType type = b2_staticBody, b2Vec2 position = b2Vec2(0, 0), float radius = 1, float density = 0, float restitution = 0, float friction = 0.3f)
	{
		return PhysicsObject(world, type, position, radius, density, restitution, friction);
	}

	PhysicsObject() {  }
	~PhysicsObject();

	b2Body* getBody() const { return body; }

private:
	PhysicsObject(b2World* world, b2BodyType type = b2_staticBody, b2Vec2 position = b2Vec2(0, 0), b2Vec2 boxSize = b2Vec2(1, 1), float density = 0, float restitution = 0, float friction = 0.3f);
	PhysicsObject(b2World* world, b2BodyType type = b2_staticBody, b2Vec2 position = b2Vec2(0, 0), float radius = 1, float density = 0, float restitution = 0, float friction = 0.3f);
	
	b2Body* body;
};

