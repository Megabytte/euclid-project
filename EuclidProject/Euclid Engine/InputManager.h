#pragma once

#include <map>
#include <string>
#include <iostream>
#include <SFML/Graphics.hpp>

class InputManager
{
public:
	static InputManager* Get()
	{
		if (s_pInstance == 0)
		{
			s_pInstance = new InputManager();
			return s_pInstance;
		}
		return s_pInstance;
	}

	bool EventTriggered(sf::Event event, sf::Event::EventType type);
	bool isKeyDown(sf::Keyboard::Key key);
    bool isKeyDownEvent(sf::Event event, sf::Keyboard::Key key);

private:
	InputManager();
	~InputManager();



	static InputManager* s_pInstance;
};
typedef InputManager TIM;


