#include "GameObject.h"

GameObject::GameObject()
{
	Animation nullA;
	animationMap.insert(std::make_pair("NULL", nullA));
}

GameObject::~GameObject()
{
    physicsObj.~PhysicsObject();
    sprite.~AnimatedSprite();
    animationMap.clear();
}

void GameObject::draw(RenderWindow& window)
{
	
	
	window.draw(this->sprite);
	return;
}

void GameObject::update(sf::Time delta)
{
	sprite.update(delta);

	return;
}

Animation& GameObject::getAnimation(std::string id)
{
	if (animationMap.find(id) == animationMap.end())
	{
		std::cout << "\nGetting Animation With ID: " << id << " Failed!\n";
		return animationMap.find("NULL")->second;
	}
	else
		return animationMap.find(id)->second;
}

void GameObject::createNewAnimation(std::string id)
{
	Animation nullA;
	animationMap.insert(std::make_pair(id, nullA));
	return;
}

void GameObject::createNewAnimation(std::string id, Animation anim)
{
	animationMap.insert(std::make_pair(id, anim));
	return;
}

void GameObject::createNewAnimation(std::string id, std::string textureID, int posX, int posY, int sizeX, int sizeY, int totalFrames, int Mode)
{
	Animation ani;
	ani.setSpriteSheet(TRM::Get()->getTexture(textureID));
	animationMap.insert(std::make_pair(id, ani));
	addFrames(id, posX, posY, sizeX, sizeY, totalFrames, Mode);
	return;
}

void GameObject::setSpriteAnimation(std::string id)
{
	if (animationMap.find(id) == animationMap.end())
	{
		std::cout << "\nSetting Animation With ID: " << id << " Failed!\n";
	}
	else
		sprite.setAnimation(animationMap.find(id)->second);
	return;
}

void GameObject::addFrameToAnimation(std::string animId, int posX, int posY, int sizeX, int sizeY)
{
	if (animationMap.find(animId) == animationMap.end())
	{
		std::cout << "\nGetting Animation With ID: " << animId << " Failed!\n";
	}
	else
		animationMap.find(animId)->second.addFrame(sf::IntRect(sf::Vector2<int>(posX, posY), sf::Vector2<int>(sizeX, sizeY)));
	return;
}

void GameObject::addFrames(std::string id, int framesSizeX, int framesSizeY, int numFramesX, int numFramesY, int totalFrames, int Mode)
{
	if (Mode == LEFT_TO_RIGHT_TOP_TO_BOTTOM)
	{
		for (int y = 0; y <= numFramesY; y++)
		{
			for (int x = 0; x <= numFramesX; x++)
			{
				if (animationMap.find(id)->second.getSize() != totalFrames)
					animationMap.find(id)->second.addFrame(sf::IntRect(sf::Vector2<int>(x * framesSizeX, y * framesSizeY), sf::Vector2<int>(framesSizeX, framesSizeY)));
			}
		}
	}
	else if (Mode == RIGHT_TO_LEFT_TOP_TO_BOTTOM)
	{
		for (int x = numFramesX; x >= 0; x--)
		{
			for (int y = numFramesY; y >= 0; y--)
			{
				animationMap.find(id)->second.addFrame(sf::IntRect(sf::Vector2<int>(x * framesSizeX, y * framesSizeY), sf::Vector2<int>(framesSizeX, framesSizeY)));
			}
		}
	}
}
