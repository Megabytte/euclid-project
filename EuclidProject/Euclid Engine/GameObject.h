#pragma once

#include <SFML/Graphics.hpp>
#include <vector>

#include "PhysicsObject.h"
#include "ResourceManager.h"
#include "Animation.h"
#include "AnimatedSprite.h"

using namespace sf;

class GameObject
{
public:
	//Create Modes of Loading in Animation from a sprite sheet
    enum ANIMATION_MODE { LEFT_TO_RIGHT_TOP_TO_BOTTOM, LEFT_TO_RIGHT_BOTTOM_TO_TOP, RIGHT_TO_LEFT_TOP_TO_BOTTOM, RIGHT_TO_LEFT_BOTTOM_TO_TOP };

	GameObject();
	
	AnimatedSprite& getSprite() { return sprite; }
	PhysicsObject& getPhysicsObject() { return physicsObj; }
	int getFrame() const { return frame; }
	void setFrame(int val) { frame = val; }

	Animation& getAnimation(std::string id);
	void setSpriteAnimation(std::string id);

	void createNewAnimation(std::string id);
	void createNewAnimation(std::string id, Animation anim);
	void createNewAnimation(std::string id, std::string textureID, int posX, int posY, int sizeX, int sizeY, int totalFrames, int Mode);
	void addFrames(std::string id, int framesSizeX, int framesSizeY, int numFramesX, int numFramesY, int totalFrames, int Mode);
	void addFrameToAnimation(std::string animId, int posX, int posY, int sizeX, int sizeY);

	void draw(RenderWindow& window);
	void update(sf::Time delta);

	~GameObject();
private:
	int frame = 0;
	PhysicsObject physicsObj;
	AnimatedSprite sprite;
	//Animation& currentAnimation;
	std::map<std::string, Animation> animationMap;
};

